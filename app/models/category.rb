class Category 
  include Neo4j::ActiveNode
  property :name, type: String

  has_many :in, :books, origin: :categories
  
  searchkick index_name: "categories_v1"
  def search_data
   {
    id: uuid,
    title: name,
    email: nil,
    year_published: nil,
    isbn: nil
    }
  end

end
