class User 
  include Neo4j::ActiveNode
  property :name, type: String
  property :email, type: String

  searchkick index_name: "users_v1"
  def search_data
   {
    id: uuid,
    title: name,
    email: email,
    year_published: nil,
    isbn: nil
    }
  end

end
