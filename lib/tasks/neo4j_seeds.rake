namespace :neo4j_seeds  do

  desc "Upload test data for performance test"
  task upload_data: :environment do
    users = []
    categories = []
    100000.times.each do |num|
      users << User.new(name: "author#{num}", email: "author#{num}@bookface.com")
      categories << Category.new(name: "Genre#{num}")
    end
    users.each(&:save)
    puts "=====Created 100000 Users====="
    categories.each(&:save)
    puts "=====Created 100000 Categories====="

    books = []
    u = User.all.pluck(:id)
    c = Category.all.pluck(:id)
    y = ['1970','1975','1980','1985','1990','1995','2000','2005','2010']
    puts "=====Adding Books to the data set=====" 
    1000000.times.each do |num|
      books << Book.new(isbn: "0#{num}", 
      title: "Book#{num}", year_published: y.sample, author: u.sample, category_ids: [c.sample, c.sample])
    end
    books.each(&:save)
    puts "=====Created 1000000 Books====="  
  end

end
