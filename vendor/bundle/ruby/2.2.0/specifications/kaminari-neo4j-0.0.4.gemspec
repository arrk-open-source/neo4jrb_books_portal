# -*- encoding: utf-8 -*-
# stub: kaminari-neo4j 0.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "kaminari-neo4j"
  s.version = "0.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Dieter Pisarewski"]
  s.date = "2016-01-13"
  s.description = "Adds Neo4j support to kaminari"
  s.email = ["dieter.pisarewski@gmail.com"]
  s.homepage = "https://github.com/megorei/kaminari-neo4j"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Neo4j support for kaminari"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>, [">= 3.0"])
      s.add_runtime_dependency(%q<neo4j>, [">= 4.0"])
      s.add_runtime_dependency(%q<kaminari>, [">= 0.16.3"])
    else
      s.add_dependency(%q<activesupport>, [">= 3.0"])
      s.add_dependency(%q<neo4j>, [">= 4.0"])
      s.add_dependency(%q<kaminari>, [">= 0.16.3"])
    end
  else
    s.add_dependency(%q<activesupport>, [">= 3.0"])
    s.add_dependency(%q<neo4j>, [">= 4.0"])
    s.add_dependency(%q<kaminari>, [">= 0.16.3"])
  end
end
