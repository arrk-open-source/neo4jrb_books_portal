# -*- encoding: utf-8 -*-
# stub: neo4j 8.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "neo4j"
  s.version = "8.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Andreas Ronge, Brian Underwood, Chris Grigg"]
  s.date = "2016-12-23"
  s.description = "A Neo4j OGM (Object-Graph-Mapper) for use in Ruby on Rails and Rack frameworks heavily inspired by ActiveRecord.\n"
  s.email = "andreas.ronge@gmail.com, public@brian-underwood.codes, chris@subvertallmedia.com"
  s.executables = ["neo4j-jars"]
  s.extra_rdoc_files = ["README.md"]
  s.files = ["README.md", "bin/neo4j-jars"]
  s.homepage = "https://github.com/neo4jrb/neo4j/"
  s.licenses = ["MIT"]
  s.rdoc_options = ["--quiet", "--title", "Neo4j.rb", "--line-numbers", "--main", "README.rdoc", "--inline-source"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.1.9")
  s.rubyforge_project = "neo4j"
  s.rubygems_version = "2.4.8"
  s.summary = "A graph database for Ruby"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<orm_adapter>, ["~> 0.5.0"])
      s.add_runtime_dependency(%q<activemodel>, ["< 5.1", ">= 4.0"])
      s.add_runtime_dependency(%q<activesupport>, ["< 5.1", ">= 4.0"])
      s.add_runtime_dependency(%q<neo4j-core>, [">= 7.0.0"])
      s.add_development_dependency(%q<railties>, ["< 5.1", ">= 4.0"])
      s.add_development_dependency(%q<pry>, [">= 0"])
      s.add_development_dependency(%q<os>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<yard>, [">= 0"])
      s.add_development_dependency(%q<guard>, [">= 0"])
      s.add_development_dependency(%q<guard-rubocop>, [">= 0"])
      s.add_development_dependency(%q<guard-rspec>, [">= 0"])
      s.add_development_dependency(%q<rubocop>, ["~> 0.39.0"])
    else
      s.add_dependency(%q<orm_adapter>, ["~> 0.5.0"])
      s.add_dependency(%q<activemodel>, ["< 5.1", ">= 4.0"])
      s.add_dependency(%q<activesupport>, ["< 5.1", ">= 4.0"])
      s.add_dependency(%q<neo4j-core>, [">= 7.0.0"])
      s.add_dependency(%q<railties>, ["< 5.1", ">= 4.0"])
      s.add_dependency(%q<pry>, [">= 0"])
      s.add_dependency(%q<os>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<yard>, [">= 0"])
      s.add_dependency(%q<guard>, [">= 0"])
      s.add_dependency(%q<guard-rubocop>, [">= 0"])
      s.add_dependency(%q<guard-rspec>, [">= 0"])
      s.add_dependency(%q<rubocop>, ["~> 0.39.0"])
    end
  else
    s.add_dependency(%q<orm_adapter>, ["~> 0.5.0"])
    s.add_dependency(%q<activemodel>, ["< 5.1", ">= 4.0"])
    s.add_dependency(%q<activesupport>, ["< 5.1", ">= 4.0"])
    s.add_dependency(%q<neo4j-core>, [">= 7.0.0"])
    s.add_dependency(%q<railties>, ["< 5.1", ">= 4.0"])
    s.add_dependency(%q<pry>, [">= 0"])
    s.add_dependency(%q<os>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<yard>, [">= 0"])
    s.add_dependency(%q<guard>, [">= 0"])
    s.add_dependency(%q<guard-rubocop>, [">= 0"])
    s.add_dependency(%q<guard-rspec>, [">= 0"])
    s.add_dependency(%q<rubocop>, ["~> 0.39.0"])
  end
end
